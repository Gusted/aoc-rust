use std::{fs::File, io::{BufReader, BufRead}, collections::HashMap, vec};

fn main() {
    let file = File::open("input").unwrap();
    let mut reader = BufReader::new(file);

    // The first line is special.
    let mut first_line = String::new();

    let _ = reader.read_line(&mut first_line);


    // The puzzle input: 12,241,312,312 etc...
    let puzzle_input = first_line.split(',');

    // Wow!
    // Top-level array: Each "puzzle board"
    // Second-level array: Each row in the puzzle-board.
    // Third-level array: The actual number within the row.
    // These only contains the numbers which aren't marked yet.
    let mut unmarked_matrixes: Vec<Vec<Vec<usize>>> = vec![vec![vec![0; 5]; 5]; 1];

    // Store number:locations in a hashmap, so we don't have to loop trough all matrixes
    // "26": [[1, 2, 3]] Means that the number 26 is in the first matrix at the second row as the third number.
    let mut number_to_locations: HashMap<usize, Vec<[usize; 3]>> = HashMap::new();

    let mut matrix_count: usize = 0;
    let mut row_count: usize = 0;
    let mut column_count: usize = 0;
    let mut boards: Vec<usize> = vec![0];
    for line_result in reader.lines() {
        let line = line_result.unwrap();

        if line.is_empty() {
            continue;
        }
        let split = line.split(' ');

        for s in split {
            if s.is_empty() {
                continue;
            }
            let number: usize;
            match s.to_string().parse::<usize>() {
                Err(..) => {
                    panic!("{:?}", s);
                },
                Ok(num) => {
                    number = num;
                }
            }
            if number_to_locations.contains_key(&number) {
                number_to_locations.get_mut(&number).unwrap().push([matrix_count, row_count, column_count]);
            } else {
                match number_to_locations.insert(number, vec![[matrix_count, row_count, column_count]]) {
                    None => {},
                    Some(present_key) => {
                        panic!("Key was already inserted, {:?}: {:?}", s, present_key);
                    }
                }
            }
            // It's always unmarked at the beginning.
            unmarked_matrixes[matrix_count][row_count][column_count] = number;

            // LOL!
            column_count += 1;
            if column_count >= 5 {
                column_count = 0;
                row_count += 1;
                if row_count >= 5 {
                    unmarked_matrixes.push(vec![vec![0; 5]; 5]);
                    matrix_count += 1;
                    boards.push(matrix_count);
                    row_count = 0;
                    column_count = 0;
                }
            }
        }
    }
    // Last vec in unmarked_matrixes is empty, removing.
    unmarked_matrixes.remove(matrix_count);
    // Remove last board, as it's empty anyways.
    boards.remove(matrix_count);

    // The same as unmarked_matrixes, but will contain the marked inputs.
    let mut marked_matrixes = vec![vec![vec![0; 5]; 5]; unmarked_matrixes.len()];

    let mut last_number: usize = 0;
    let mut last_board: usize = 0;

    'outer_loop:
    for input in puzzle_input {
        let number: usize;
        match input.to_string().trim().parse::<usize>() {
            Err(..) => {
                panic!("{:?}", input);
            },
            Ok(num) => {
                number = num;
            }
        }

        // Check if 
        if number_to_locations.contains_key(&number) {
            last_number = number;
            let locations = number_to_locations.get_mut(&number).unwrap();

            for location in locations {
                last_board = location[0];
                marked_matrixes[location[0]][location[1]][location[2]] = 1;

                let mut board_has_won = false;
                // Check for row.
                let mut one_count = 0;
                for i in 0..5 {
                    if marked_matrixes[location[0]][location[1]][i] == 0 {
                        break;
                    }
                    one_count += 1;
                }
                if one_count == 5 {
                    board_has_won = true;
                }
                one_count = 0;
                
                // Check for column
                for i in 0..5 {
                    if marked_matrixes[location[0]][i][location[2]] == 0 {
                        break;
                    }
                    one_count += 1;
                }
                if one_count == 5 {
                    board_has_won = true;
                }

                if board_has_won {
                    if boards.len() == 1 {
                        if boards[0] == last_board {
                            break 'outer_loop;
                        }
                    } else {
                        match boards.iter().position(|x| *x == last_board) {
                            None => {},
                            Some(idx) => {
                                boards.remove(idx);
                            }
                        }
                    }

                }
            }

            number_to_locations.remove(&number).unwrap();
        }
        
    }
    let mut sum_unmarked = 0;

    for i in 0..5 {
        for j in 0..5 {
            if marked_matrixes[last_board][i][j] == 0 {
                sum_unmarked += unmarked_matrixes[last_board][i][j]
            }
        }
    }

    println!("Last number: {}, Sum of all unmarked numbers: {}, Answer: {}", last_number, sum_unmarked, last_number * sum_unmarked)
}
