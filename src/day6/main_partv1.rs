use std::{fs, collections::HashMap, ops::{AddAssign}};
fn main() {
    let file = fs::read_to_string("input").unwrap();
    let input = file.lines().nth(0).unwrap();
    let mut timer_to_amount_fish: HashMap<usize, usize> = HashMap::new();
    for number in input.split(',') {
        let num = number.parse::<usize>().unwrap();
        if timer_to_amount_fish.contains_key(&num) {
            timer_to_amount_fish.get_mut(&num).unwrap().add_assign(1);
        } else {
            match timer_to_amount_fish.insert(num, 1) {
                Some(_) => panic!("Tried to insert key which is already inserted"),
                _ => (),
            }
        }
    }
    // Loop trough the 80 days.
    for _ in 0..80 {
        let cloned_timer_to_amount_fish = timer_to_amount_fish.clone();
        timer_to_amount_fish.clear();
        
        for (key, value) in cloned_timer_to_amount_fish.iter() {
            if *key == 0 {
                match timer_to_amount_fish.insert(8, *value) {
                    Some(_) => panic!("Tried to insert key which is already inserted"),
                    _ => (),
                }
                if timer_to_amount_fish.contains_key(&6) {
                    timer_to_amount_fish.get_mut(&6).unwrap().add_assign(*value);
                } else {
                    match timer_to_amount_fish.insert(6, *value) {
                        Some(_) => panic!("Tried to insert key which is already inserted"),
                        _ => (),
                    }
                }
            } else {
                if timer_to_amount_fish.contains_key(&((*key)-1)) {
                    timer_to_amount_fish.get_mut(&((*key)-1)).unwrap().add_assign(*value);
                } else {
                    match timer_to_amount_fish.insert((*key)-1, *value) {
                        Some(_) => panic!("Tried to insert key which is already inserted"),
                        _ => (),
                    }
                }
            }
        }
    }
    let mut fishes = 0;
    for value in timer_to_amount_fish.values() {
        fishes += value;
    }
    println!("{:?}: {}", timer_to_amount_fish, fishes)
}