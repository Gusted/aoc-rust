use std::{fs, vec, collections::HashMap};

fn main() {
    let file = fs::read_to_string("input").unwrap();

    let mut result_number = 0;
    for line in file.lines() {
        let mut splitted = line.split(" | ");
        let signal_info = splitted.next().unwrap();
        let output_number = splitted.next().unwrap();

        // We will first loop trough the signal_info.
        // This should gives us all the combinations for the numbers,
        // but we don't know the numbers, but we can find out trough
        // looking at where the unique numbers(1, 4, 7, 8) cross
        // And than play a little game :)
        //
        // We will go trough all segments and we want to know
        // Which are 1, 4, 7, 8. Hereby we can quickly know
        // which are C and F(or visa versa, which requires further deduction).
        // Because of 7, we will be knowing which signal corresponds to A.

        let mut is_one: Vec<&str> = vec![];
        let mut is_four: Vec<&str> = vec![];
        let mut is_seven: Vec<&str> = vec![];
        let mut is_eight: Vec<&str> = vec![];
        let mut is_zero_six_nine: Vec<Vec<&str>> = vec![];
        let mut is_two_three_five: Vec<Vec<&str>> = vec![];

        for segment in signal_info.split(' ') {
            match segment.len() {
                2 => {
                    is_one = segment
                        .split("")
                        .filter(|ch| !ch.is_empty())
                        .collect::<Vec<_>>();
                }
                3 => {
                    is_seven = segment
                        .split("")
                        .filter(|ch| !ch.is_empty())
                        .collect::<Vec<_>>();
                }
                4 => {
                    is_four = segment
                        .split("")
                        .filter(|ch| !ch.is_empty())
                        .collect::<Vec<_>>();
                }
                5 => {
                    is_two_three_five.push(
                        segment
                            .split("")
                            .filter(|ch| !ch.is_empty())
                            .collect::<Vec<_>>(),
                    );
                }
                6 => {
                    is_zero_six_nine.push(
                        segment
                            .split("")
                            .filter(|ch| !ch.is_empty())
                            .collect::<Vec<_>>(),
                    );
                }
                7 => {
                    is_eight = segment
                        .split("")
                        .filter(|ch| !ch.is_empty())
                        .collect::<Vec<_>>();
                }
                _ => {
                    panic!("WTF!")
                }
            }
        }

        // Get d and e corresponding key, by fetching 0, 9, 6.
        // They have all keys except one (0: d 9: e 6: c).
        // We  them all in 1 vector, so 6 gives us the value of c,
        // we know which one is c, because we know which one is 1.
        // Which has CF and 0/9 has the CF keys.

        // let all_keys = vec!["a", "b", "c", "d", "e", "f", "g"];

        let one_first_part = is_one.clone().iter().next().unwrap().to_string();
        let one_second_part = is_one.clone().iter().nth(1).unwrap().to_string();

        let mut is_zero_six_nine_cloned = is_zero_six_nine.clone();
        let mut is_six = is_zero_six_nine_cloned
            .iter()
            .filter(|sen| {
                !(sen.clone().iter().any(|&x| x == one_first_part)
                    && sen.clone().iter().any(|&x| x == one_second_part))
            })
            .nth(0)
            .unwrap()
            .to_owned();

        is_zero_six_nine_cloned = is_zero_six_nine.clone();

        let four_1_part = is_four.clone().iter().next().unwrap().to_string();
        let four_2_part = is_four.clone().iter().nth(1).unwrap().to_string();
        let four_3_part = is_four.clone().iter().nth(2).unwrap().to_string();
        let four_4_part = is_four.clone().iter().nth(3).unwrap().to_string();

        let mut is_nine = is_zero_six_nine_cloned
            .iter()
            .filter(|sen| {
                sen.clone().iter().any(|&x| x == four_1_part)
                    && sen.clone().iter().any(|&x| x == four_2_part)
                    && sen.clone().iter().any(|&x| x == four_3_part)
                    && sen.clone().iter().any(|&x| x == four_4_part)
            })
            .nth(0)
            .unwrap()
            .to_owned();

        is_zero_six_nine_cloned = is_zero_six_nine.clone();

        let mut is_zero = is_zero_six_nine_cloned
            .iter()
            .filter(|sen| (*sen.clone() != is_nine && *sen.clone() != is_six))
            .nth(0)
            .unwrap()
            .to_owned();

        let mut is_two_three_five_cloned = is_two_three_five.clone();
        let mut is_three = is_two_three_five_cloned
            .iter()
            .filter(|sen| {
                sen.clone().iter().any(|&x| x == one_first_part)
                    && sen.clone().iter().any(|&x| x == one_second_part)
            })
            .nth(0)
            .unwrap()
            .to_owned();

        let mut is_five = is_two_three_five_cloned
            .iter()
            .filter(|sen| {
                if *sen.clone() != is_three {
                    let mut matches = 0;
                    if sen.clone().iter().any(|&x| x == four_1_part) == true {
                        matches += 1;
                    }
                    if sen.clone().iter().any(|&x| x == four_2_part) == true {
                        matches += 1;
                    }
                    if sen.clone().iter().any(|&x| x == four_3_part) == true {
                        matches += 1;
                    }
                    if sen.clone().iter().any(|&x| x == four_4_part) == true {
                        matches += 1;
                    }
                    matches == 3
                } else {
                    false
                }
            })
            .nth(0)
            .unwrap()
            .to_owned();

        is_two_three_five_cloned = is_two_three_five.clone();

        let mut is_two = is_two_three_five_cloned
            .iter()
            .filter(|sen| (*sen.clone() != is_five && *sen.clone() != is_three))
            .nth(0)
            .unwrap()
            .to_owned();
        
        is_zero.sort();
        is_one.sort();
        is_two.sort();
        is_three.sort();
        is_four.sort();
        is_five.sort();
        is_six.sort();
        is_seven.sort();
        is_eight.sort();
        is_nine.sort();

        let mut signal_to_digit: HashMap<String, usize> = HashMap::new();
        signal_to_digit.insert(is_zero.join(""), 0);
        signal_to_digit.insert(is_one.join(""), 1);
        signal_to_digit.insert(is_two.join(""), 2);
        signal_to_digit.insert(is_three.join(""), 3);
        signal_to_digit.insert(is_four.join(""), 4);
        signal_to_digit.insert(is_five.join(""), 5);
        signal_to_digit.insert(is_six.join(""), 6);
        signal_to_digit.insert(is_seven.join(""), 7);
        signal_to_digit.insert(is_eight.join(""), 8);
        signal_to_digit.insert(is_nine.join(""), 9);

        let mut idx = 4;
        let mut process_number = 0;
        for number in output_number.split(' ').filter(|x| !x.is_empty()) {
            let mut chars: Vec<char> = number.chars().collect();
            chars.sort();
            let s = String::from_iter(chars);
            let corresponding_number = signal_to_digit.get(&s).unwrap();
            println!("{} : {:?}", s, signal_to_digit);
            process_number += corresponding_number * 10_usize.pow(idx-1);
            idx -= 1;
        }
        result_number += process_number;
    }

    println!("Final result: {}", result_number)
}
