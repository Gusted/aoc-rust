use std::fs;

fn main() {
    let file = fs::read_to_string("input").unwrap();

    let mut possible_digts = 0;

    for line in file.lines() {
        let signal_info = line.split(" | ").nth(1).unwrap();
        for segment in signal_info.split(' ') {
            match segment.len() {
                2 | 3 | 4 | 7 => {
                    possible_digts += 1;
                }
                _ => (),
            }
        }
    }

    println!("Amount of 1, 4, 7, 8: {}", possible_digts)
}
