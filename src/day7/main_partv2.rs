use std::{
    collections::HashMap,
    fs,
    ops::{AddAssign, Div},
};
fn fibonacci_v2_0(nth: i32) -> i32 {
    return nth * (nth + 1) / 2;
}

fn main() {
    let file = fs::read_to_string("input").unwrap();
    let mut pos_to_amount_fish: HashMap<usize, usize> = HashMap::new();
    
    for number in file.split(',') {
        let num = number.parse::<usize>().unwrap();
        if pos_to_amount_fish.contains_key(&num) {
            pos_to_amount_fish.get_mut(&num).unwrap().add_assign(1);
        } else {
            match pos_to_amount_fish.insert(num, 1) {
                Some(_) => panic!("Tried to insert key which is already inserted"),
                _ => (),
            }
        }
    }

    let mut amount_of_positons = 0;
    let mut all_values = 0;
    for (key, value) in pos_to_amount_fish.clone().into_iter() {
        all_values += value * key;
        amount_of_positons += value;
    }

    let mut cost_fuel = 0;
    let pos = all_values.div(amount_of_positons) as i32;
    for (key, value) in pos_to_amount_fish.clone().into_iter() {
        cost_fuel += fibonacci_v2_0((key as i32 - pos).abs()) * value as i32;
    }

    println!(
        "Cost_fuel: {} | Pos: {}",
        cost_fuel,
        pos
    );
}
