use std::{fs::File, io::{BufReader, BufRead}, collections::HashMap, ops::{AddAssign}, panic, cmp};

fn main() {
    let file = File::open("input").unwrap();
    let reader = BufReader::new(file);

    let mut matrix: HashMap<[usize; 2], usize> = HashMap::new();

    for line_result in reader.lines() {
        let line = line_result.unwrap();
        if line.is_empty() {
            continue;
        }

        let mut split = line.split(" -> ");
        let mut start_pos = split.nth(0).unwrap().split(',');
        let mut end_pos = split.nth(0).unwrap().split(',');
        let x1 = start_pos.nth(0).unwrap().parse::<usize>().unwrap();
        let y1 = start_pos.nth(0).unwrap().parse::<usize>().unwrap();
        let x2 = end_pos.nth(0).unwrap().parse::<usize>().unwrap();
        let y2 = end_pos.nth(0).unwrap().parse::<usize>().unwrap();

        if x1 != x2 && y1 != y2 {
            let min_value;
            let max_value;
            let increase_by;
            let mut y_value;
            if x1 > x2 {
                y_value = y2;
                max_value = x1;
                min_value = x2;
                increase_by = y1 > y2;
            } else {
                y_value = y1;
                max_value = x2;
                min_value = x1;
                increase_by = y2 > y1;
            }

            for i in min_value..max_value+1 {
                if matrix.contains_key(&[i, y_value]) {
                    matrix.get_mut(&[i, y_value]).unwrap().add_assign(1);
                } else {
                    match matrix.insert([i, y_value], 1) {
                        Some(_) => panic!("called `Option::unwrap()` on a `Some` value"),
                        None => (),
                    }
                }
                if increase_by {
                    y_value += 1;
                } else {
                    y_value -= 1;
                }
            }
        } else if x1 == x2 {
            let max_val = cmp::max(y1, y2);
            let min_val = cmp::min(y1, y2);
            for i in min_val..max_val+1 {
                if matrix.contains_key(&[x1, i]) {
                    matrix.get_mut(&[x1, i]).unwrap().add_assign(1);
                } else {
                    match matrix.insert([x1, i], 1) {
                        Some(_) => panic!("called `Option::unwrap()` on a `Some` value"),
                        None => (),
                    }
                }
            }
        } else if y1 == y2 {
            let max_val = cmp::max(x1, x2);
            let min_val = cmp::min(x1, x2);
            for i in min_val..max_val+1 {
                if matrix.contains_key(&[i, y1]) {
                    matrix.get_mut(&[i, y1]).unwrap().add_assign(1);
                } else {
                    match matrix.insert([i, y1], 1) {
                        Some(_) => panic!("called `Option::unwrap()` on a `Some` value"),
                        None => (),
                    }
                }
            }
        } else {
            panic!("WTF!")
        }
    }

    let mut amounts_of_two_or_more = 0;
    for val in matrix.into_values() {
        if val >= 2 {
            amounts_of_two_or_more += 1;
        }
    }
    println!("LOL! {}", amounts_of_two_or_more)
}
